library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library work;
use work.txt_util.ALL;

entity NoC_tb_microblaze is
end NoC_tb_microblaze;

architecture Behavioral of NoC_tb_microblaze is
    signal rst : STD_LOGIC := '1';
    signal clk_p : STD_LOGIC := '0';
    signal clk_n : STD_LOGIC := '1';
    
    signal uart_txd : STD_LOGIC;
    signal uart_rxd : STD_LOGIC := '0';
begin

    uut: entity work.design_1_wrapper
        port map (CLK_IN1_D_clk_p => clk_p,
                  CLK_IN1_D_clk_n => clk_n,
                  reset => rst,
                  uart_rtl_txd => uart_txd,
                  uart_rtl_rxd => uart_rxd);

    clock: process begin
        clk_p <= not clk_p;
        clk_n <= not clk_n;
        wait for 2.5 ns;
    end process;

    test: process begin
        wait for 100 ns;
        rst <= '0';

        wait;
    end process;

    -- uart receiver for 115200 bps
    uart_recv: process
        variable recv_byte : unsigned(7 downto 0);
    begin
        -- wait for start bit
        wait until falling_edge(uart_txd);

        -- wait half-bit time
        wait for 4340 ns;

        -- receive 8 bits
        recv_byte := (others => '0');
        for i in 0 to 7 loop
            wait for 8680 ns;
            recv_byte := recv_byte srl 1;
            recv_byte := recv_byte or (uart_txd & "0000000");
        end loop;

        -- wait stop bit time
        wait for 8680 ns;

        -- print byte
        print("uart received '" & character'val(to_integer(recv_byte)) & "'");

    end process;

end Behavioral;
