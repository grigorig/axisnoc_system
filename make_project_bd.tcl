
################################################################
# This is a generated script based on design: design_1
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2013.4
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   puts "ERROR: This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source design_1_script.tcl

# If you do not already have a project created,
# you can create a project using the following command:
#    create_project project_1 myproj -part xc7a200tfbg676-2


# CHANGE DESIGN NAME HERE
set design_name design_1

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# CHECKING IF PROJECT EXISTS
if { [get_projects -quiet] eq "" } {
   puts "ERROR: Please open or create a project!"
   return 1
}


# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
if { ${design_name} ne "" && ${cur_design} eq ${design_name} } {
   # Checks if design is empty or not
   set list_cells [get_bd_cells -quiet]

   if { $list_cells ne "" } {
      set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
      set nRet 1
   } else {
      puts "INFO: Constructing design in IPI design <$design_name>..."
   }
} else {

   if { [get_files -quiet ${design_name}.bd] eq "" } {
      puts "INFO: Currently there is no design <$design_name> in project, so creating one..."

      create_bd_design $design_name

      puts "INFO: Making design <$design_name> as current_bd_design."
      current_bd_design $design_name

   } else {
      set errMsg "ERROR: Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
      set nRet 3
   }

}

puts "INFO: Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   puts $errMsg
   return $nRet
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: noc_2x2
proc create_hier_cell_noc_2x2 { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_noc_2x2() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M00_AXIS
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M00_AXIS1
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M00_AXIS2
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M00_AXIS3
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M00_AXIS4
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M00_AXIS5
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M00_AXIS6
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M00_AXIS7
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 S00_AXIS
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 S00_AXIS1
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 S00_AXIS2
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 S00_AXIS3
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 S00_AXIS4
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 S00_AXIS5
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 S00_AXIS6
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 S00_AXIS7

  # Create pins
  create_bd_pin -dir I clk
  create_bd_pin -dir I m00_axis_aclk
  create_bd_pin -dir I -from 0 -to 0 m00_axis_aresetn
  create_bd_pin -dir I -from 11 -to 0 mon_addr
  create_bd_pin -dir O -from 31 -to 0 mon_data
  create_bd_pin -dir I -from 0 -to 0 rst

  # Create instance: axisnoc_interface_0, and set properties
  set axisnoc_interface_0 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_interface:1.0 axisnoc_interface_0 ]

  # Create instance: axisnoc_interface_1, and set properties
  set axisnoc_interface_1 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_interface:1.0 axisnoc_interface_1 ]

  # Create instance: axisnoc_interface_2, and set properties
  set axisnoc_interface_2 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_interface:1.0 axisnoc_interface_2 ]

  # Create instance: axisnoc_interface_3, and set properties
  set axisnoc_interface_3 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_interface:1.0 axisnoc_interface_3 ]

  # Create instance: axisnoc_interface_4, and set properties
  set axisnoc_interface_4 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_interface:1.0 axisnoc_interface_4 ]

  # Create instance: axisnoc_interface_5, and set properties
  set axisnoc_interface_5 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_interface:1.0 axisnoc_interface_5 ]

  # Create instance: axisnoc_interface_6, and set properties
  set axisnoc_interface_6 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_interface:1.0 axisnoc_interface_6 ]

  # Create instance: axisnoc_interface_7, and set properties
  set axisnoc_interface_7 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_interface:1.0 axisnoc_interface_7 ]

  # Create instance: axisnoc_router_0, and set properties
  set axisnoc_router_0 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_router:1.0 axisnoc_router_0 ]
  set_property -dict [ list CONFIG.ADDR {"00010000"} CONFIG.ENABLE_MONITOR {true} CONFIG.LOCAL_INPUT_MASK {"00010101"} CONFIG.LOCAL_OUTPUT_MASK {"00011010"}  ] $axisnoc_router_0

  # Create instance: axisnoc_router_1, and set properties
  set axisnoc_router_1 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_router:1.0 axisnoc_router_1 ]
  set_property -dict [ list CONFIG.ADDR {"00010001"} CONFIG.LOCAL_INPUT_MASK {"00010110"} CONFIG.LOCAL_OUTPUT_MASK {"00011001"}  ] $axisnoc_router_1

  # Create instance: axisnoc_router_2, and set properties
  set axisnoc_router_2 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_router:1.0 axisnoc_router_2 ]
  set_property -dict [ list CONFIG.ADDR {"00100000"} CONFIG.LOCAL_INPUT_MASK {"00011001"} CONFIG.LOCAL_OUTPUT_MASK {"00010110"}  ] $axisnoc_router_2

  # Create instance: axisnoc_router_3, and set properties
  set axisnoc_router_3 [ create_bd_cell -type ip -vlnv axisnoc:user:axisnoc_router:1.0 axisnoc_router_3 ]
  set_property -dict [ list CONFIG.ADDR {"00100001"} CONFIG.LOCAL_INPUT_MASK {"00011010"} CONFIG.LOCAL_OUTPUT_MASK {"00010101"}  ] $axisnoc_router_3

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.0 xlconstant_0 ]
  set_property -dict [ list CONFIG.CONST_VAL {0} CONFIG.CONST_WIDTH {32}  ] $xlconstant_0

  # Create instance: xlconstant_1, and set properties
  set xlconstant_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.0 xlconstant_1 ]
  set_property -dict [ list CONFIG.CONST_VAL {0} CONFIG.CONST_WIDTH {32}  ] $xlconstant_1

  # Create instance: xlconstant_2, and set properties
  set xlconstant_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.0 xlconstant_2 ]
  set_property -dict [ list CONFIG.CONST_VAL {0} CONFIG.CONST_WIDTH {32}  ] $xlconstant_2

  # Create instance: xlconstant_3, and set properties
  set xlconstant_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.0 xlconstant_3 ]
  set_property -dict [ list CONFIG.CONST_VAL {0} CONFIG.CONST_WIDTH {32}  ] $xlconstant_3

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins S00_AXIS] [get_bd_intf_pins axisnoc_interface_0/S00_AXIS]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins M00_AXIS] [get_bd_intf_pins axisnoc_interface_0/M00_AXIS]
  connect_bd_intf_net -intf_net Conn3 [get_bd_intf_pins S00_AXIS1] [get_bd_intf_pins axisnoc_interface_1/S00_AXIS]
  connect_bd_intf_net -intf_net Conn4 [get_bd_intf_pins M00_AXIS1] [get_bd_intf_pins axisnoc_interface_1/M00_AXIS]
  connect_bd_intf_net -intf_net Conn5 [get_bd_intf_pins S00_AXIS2] [get_bd_intf_pins axisnoc_interface_2/S00_AXIS]
  connect_bd_intf_net -intf_net Conn6 [get_bd_intf_pins M00_AXIS2] [get_bd_intf_pins axisnoc_interface_2/M00_AXIS]
  connect_bd_intf_net -intf_net Conn7 [get_bd_intf_pins S00_AXIS3] [get_bd_intf_pins axisnoc_interface_3/S00_AXIS]
  connect_bd_intf_net -intf_net Conn8 [get_bd_intf_pins M00_AXIS3] [get_bd_intf_pins axisnoc_interface_3/M00_AXIS]
  connect_bd_intf_net -intf_net Conn9 [get_bd_intf_pins S00_AXIS4] [get_bd_intf_pins axisnoc_interface_4/S00_AXIS]
  connect_bd_intf_net -intf_net Conn10 [get_bd_intf_pins M00_AXIS4] [get_bd_intf_pins axisnoc_interface_4/M00_AXIS]
  connect_bd_intf_net -intf_net Conn11 [get_bd_intf_pins S00_AXIS5] [get_bd_intf_pins axisnoc_interface_5/S00_AXIS]
  connect_bd_intf_net -intf_net Conn12 [get_bd_intf_pins M00_AXIS5] [get_bd_intf_pins axisnoc_interface_5/M00_AXIS]
  connect_bd_intf_net -intf_net Conn13 [get_bd_intf_pins S00_AXIS6] [get_bd_intf_pins axisnoc_interface_6/S00_AXIS]
  connect_bd_intf_net -intf_net Conn14 [get_bd_intf_pins M00_AXIS6] [get_bd_intf_pins axisnoc_interface_6/M00_AXIS]
  connect_bd_intf_net -intf_net Conn15 [get_bd_intf_pins M00_AXIS7] [get_bd_intf_pins axisnoc_interface_7/M00_AXIS]
  connect_bd_intf_net -intf_net Conn16 [get_bd_intf_pins S00_AXIS7] [get_bd_intf_pins axisnoc_interface_7/S00_AXIS]

  # Create port connections
  connect_bd_net -net axisnoc_interface_0_m00_noc_ready [get_bd_pins axisnoc_interface_0/m00_noc_ready] [get_bd_pins axisnoc_router_0/out4_ready]
  connect_bd_net -net axisnoc_interface_0_s00_noc_data [get_bd_pins axisnoc_interface_0/s00_noc_data] [get_bd_pins axisnoc_router_0/in4_data]
  connect_bd_net -net axisnoc_interface_0_s00_noc_valid [get_bd_pins axisnoc_interface_0/s00_noc_valid] [get_bd_pins axisnoc_router_0/in4_valid]
  connect_bd_net -net axisnoc_interface_0_s00_noc_vc [get_bd_pins axisnoc_interface_0/s00_noc_vc] [get_bd_pins axisnoc_router_0/in4_vc]
  connect_bd_net -net axisnoc_interface_1_m00_noc_ready [get_bd_pins axisnoc_interface_1/m00_noc_ready] [get_bd_pins axisnoc_router_1/out4_ready]
  connect_bd_net -net axisnoc_interface_1_s00_noc_data [get_bd_pins axisnoc_interface_1/s00_noc_data] [get_bd_pins axisnoc_router_1/in4_data]
  connect_bd_net -net axisnoc_interface_1_s00_noc_valid [get_bd_pins axisnoc_interface_1/s00_noc_valid] [get_bd_pins axisnoc_router_1/in4_valid]
  connect_bd_net -net axisnoc_interface_1_s00_noc_vc [get_bd_pins axisnoc_interface_1/s00_noc_vc] [get_bd_pins axisnoc_router_1/in4_vc]
  connect_bd_net -net axisnoc_interface_2_m00_noc_ready [get_bd_pins axisnoc_interface_2/m00_noc_ready] [get_bd_pins axisnoc_router_2/out4_ready]
  connect_bd_net -net axisnoc_interface_2_s00_noc_data [get_bd_pins axisnoc_interface_2/s00_noc_data] [get_bd_pins axisnoc_router_2/in4_data]
  connect_bd_net -net axisnoc_interface_2_s00_noc_valid [get_bd_pins axisnoc_interface_2/s00_noc_valid] [get_bd_pins axisnoc_router_2/in4_valid]
  connect_bd_net -net axisnoc_interface_2_s00_noc_vc [get_bd_pins axisnoc_interface_2/s00_noc_vc] [get_bd_pins axisnoc_router_2/in4_vc]
  connect_bd_net -net axisnoc_interface_3_m00_noc_ready [get_bd_pins axisnoc_interface_3/m00_noc_ready] [get_bd_pins axisnoc_router_3/out4_ready]
  connect_bd_net -net axisnoc_interface_3_s00_noc_data [get_bd_pins axisnoc_interface_3/s00_noc_data] [get_bd_pins axisnoc_router_3/in4_data]
  connect_bd_net -net axisnoc_interface_3_s00_noc_valid [get_bd_pins axisnoc_interface_3/s00_noc_valid] [get_bd_pins axisnoc_router_3/in4_valid]
  connect_bd_net -net axisnoc_interface_3_s00_noc_vc [get_bd_pins axisnoc_interface_3/s00_noc_vc] [get_bd_pins axisnoc_router_3/in4_vc]
  connect_bd_net -net axisnoc_interface_4_m00_noc_ready [get_bd_pins axisnoc_interface_4/m00_noc_ready] [get_bd_pins axisnoc_router_2/out2_ready]
  connect_bd_net -net axisnoc_interface_4_s00_noc_data [get_bd_pins axisnoc_interface_4/s00_noc_data] [get_bd_pins axisnoc_router_2/in3_data]
  connect_bd_net -net axisnoc_interface_4_s00_noc_valid [get_bd_pins axisnoc_interface_4/s00_noc_valid] [get_bd_pins axisnoc_router_2/in3_valid]
  connect_bd_net -net axisnoc_interface_4_s00_noc_vc [get_bd_pins axisnoc_interface_4/s00_noc_vc] [get_bd_pins axisnoc_router_2/in3_vc]
  connect_bd_net -net axisnoc_interface_5_m00_noc_ready [get_bd_pins axisnoc_interface_5/m00_noc_ready] [get_bd_pins axisnoc_router_3/out2_ready]
  connect_bd_net -net axisnoc_interface_5_s00_noc_data [get_bd_pins axisnoc_interface_5/s00_noc_data] [get_bd_pins axisnoc_router_3/in3_data]
  connect_bd_net -net axisnoc_interface_5_s00_noc_valid [get_bd_pins axisnoc_interface_5/s00_noc_valid] [get_bd_pins axisnoc_router_3/in3_valid]
  connect_bd_net -net axisnoc_interface_5_s00_noc_vc [get_bd_pins axisnoc_interface_5/s00_noc_vc] [get_bd_pins axisnoc_router_3/in3_vc]
  connect_bd_net -net axisnoc_interface_6_m00_noc_ready [get_bd_pins axisnoc_interface_6/m00_noc_ready] [get_bd_pins axisnoc_router_1/out3_ready]
  connect_bd_net -net axisnoc_interface_6_s00_noc_data [get_bd_pins axisnoc_interface_6/s00_noc_data] [get_bd_pins axisnoc_router_1/in2_data]
  connect_bd_net -net axisnoc_interface_6_s00_noc_valid [get_bd_pins axisnoc_interface_6/s00_noc_valid] [get_bd_pins axisnoc_router_1/in2_valid]
  connect_bd_net -net axisnoc_interface_6_s00_noc_vc [get_bd_pins axisnoc_interface_6/s00_noc_vc] [get_bd_pins axisnoc_router_1/in2_vc]
  connect_bd_net -net axisnoc_interface_7_m00_noc_ready [get_bd_pins axisnoc_interface_7/m00_noc_ready] [get_bd_pins axisnoc_router_0/out3_ready]
  connect_bd_net -net axisnoc_interface_7_s00_noc_data [get_bd_pins axisnoc_interface_7/s00_noc_data] [get_bd_pins axisnoc_router_0/in2_data]
  connect_bd_net -net axisnoc_interface_7_s00_noc_valid [get_bd_pins axisnoc_interface_7/s00_noc_valid] [get_bd_pins axisnoc_router_0/in2_valid]
  connect_bd_net -net axisnoc_interface_7_s00_noc_vc [get_bd_pins axisnoc_interface_7/s00_noc_vc] [get_bd_pins axisnoc_router_0/in2_vc]
  connect_bd_net -net axisnoc_router_0_in1_ready [get_bd_pins axisnoc_router_0/in1_ready] [get_bd_pins axisnoc_router_1/out1_ready]
  connect_bd_net -net axisnoc_router_0_in2_ready [get_bd_pins axisnoc_interface_7/s00_noc_ready] [get_bd_pins axisnoc_router_0/in2_ready]
  connect_bd_net -net axisnoc_router_0_in3_ready [get_bd_pins axisnoc_router_0/in3_ready] [get_bd_pins axisnoc_router_2/out3_ready]
  connect_bd_net -net axisnoc_router_0_in4_ready [get_bd_pins axisnoc_interface_0/s00_noc_ready] [get_bd_pins axisnoc_router_0/in4_ready]
  connect_bd_net -net axisnoc_router_0_mon_data [get_bd_pins mon_data] [get_bd_pins axisnoc_router_0/mon_data]
  connect_bd_net -net axisnoc_router_0_out0_data [get_bd_pins axisnoc_router_0/out0_data] [get_bd_pins axisnoc_router_1/in0_data]
  connect_bd_net -net axisnoc_router_0_out0_valid [get_bd_pins axisnoc_router_0/out0_valid] [get_bd_pins axisnoc_router_1/in0_valid]
  connect_bd_net -net axisnoc_router_0_out0_vc [get_bd_pins axisnoc_router_0/out0_vc] [get_bd_pins axisnoc_router_1/in0_vc]
  connect_bd_net -net axisnoc_router_0_out2_data [get_bd_pins axisnoc_router_0/out2_data] [get_bd_pins axisnoc_router_2/in2_data]
  connect_bd_net -net axisnoc_router_0_out2_valid [get_bd_pins axisnoc_router_0/out2_valid] [get_bd_pins axisnoc_router_2/in2_valid]
  connect_bd_net -net axisnoc_router_0_out2_vc [get_bd_pins axisnoc_router_0/out2_vc] [get_bd_pins axisnoc_router_2/in2_vc]
  connect_bd_net -net axisnoc_router_0_out3_data [get_bd_pins axisnoc_interface_7/m00_noc_data] [get_bd_pins axisnoc_router_0/out3_data]
  connect_bd_net -net axisnoc_router_0_out3_valid [get_bd_pins axisnoc_interface_7/m00_noc_valid] [get_bd_pins axisnoc_router_0/out3_valid]
  connect_bd_net -net axisnoc_router_0_out3_vc [get_bd_pins axisnoc_interface_7/m00_noc_vc] [get_bd_pins axisnoc_router_0/out3_vc]
  connect_bd_net -net axisnoc_router_0_out4_data [get_bd_pins axisnoc_interface_0/m00_noc_data] [get_bd_pins axisnoc_router_0/out4_data]
  connect_bd_net -net axisnoc_router_0_out4_valid [get_bd_pins axisnoc_interface_0/m00_noc_valid] [get_bd_pins axisnoc_router_0/out4_valid]
  connect_bd_net -net axisnoc_router_0_out4_vc [get_bd_pins axisnoc_interface_0/m00_noc_vc] [get_bd_pins axisnoc_router_0/out4_vc]
  connect_bd_net -net axisnoc_router_1_in0_ready [get_bd_pins axisnoc_router_0/out0_ready] [get_bd_pins axisnoc_router_1/in0_ready]
  connect_bd_net -net axisnoc_router_1_in2_ready [get_bd_pins axisnoc_interface_6/s00_noc_ready] [get_bd_pins axisnoc_router_1/in2_ready]
  connect_bd_net -net axisnoc_router_1_in3_ready [get_bd_pins axisnoc_router_1/in3_ready] [get_bd_pins axisnoc_router_3/out3_ready]
  connect_bd_net -net axisnoc_router_1_in4_ready [get_bd_pins axisnoc_interface_1/s00_noc_ready] [get_bd_pins axisnoc_router_1/in4_ready]
  connect_bd_net -net axisnoc_router_1_out1_data [get_bd_pins axisnoc_router_0/in1_data] [get_bd_pins axisnoc_router_1/out1_data]
  connect_bd_net -net axisnoc_router_1_out1_valid [get_bd_pins axisnoc_router_0/in1_valid] [get_bd_pins axisnoc_router_1/out1_valid]
  connect_bd_net -net axisnoc_router_1_out1_vc [get_bd_pins axisnoc_router_0/in1_vc] [get_bd_pins axisnoc_router_1/out1_vc]
  connect_bd_net -net axisnoc_router_1_out2_data [get_bd_pins axisnoc_router_1/out2_data] [get_bd_pins axisnoc_router_3/in2_data]
  connect_bd_net -net axisnoc_router_1_out2_valid [get_bd_pins axisnoc_router_1/out2_valid] [get_bd_pins axisnoc_router_3/in2_valid]
  connect_bd_net -net axisnoc_router_1_out2_vc [get_bd_pins axisnoc_router_1/out2_vc] [get_bd_pins axisnoc_router_3/in2_vc]
  connect_bd_net -net axisnoc_router_1_out3_data [get_bd_pins axisnoc_interface_6/m00_noc_data] [get_bd_pins axisnoc_router_1/out3_data]
  connect_bd_net -net axisnoc_router_1_out3_valid [get_bd_pins axisnoc_interface_6/m00_noc_valid] [get_bd_pins axisnoc_router_1/out3_valid]
  connect_bd_net -net axisnoc_router_1_out3_vc [get_bd_pins axisnoc_interface_6/m00_noc_vc] [get_bd_pins axisnoc_router_1/out3_vc]
  connect_bd_net -net axisnoc_router_1_out4_data [get_bd_pins axisnoc_interface_1/m00_noc_data] [get_bd_pins axisnoc_router_1/out4_data]
  connect_bd_net -net axisnoc_router_1_out4_valid [get_bd_pins axisnoc_interface_1/m00_noc_valid] [get_bd_pins axisnoc_router_1/out4_valid]
  connect_bd_net -net axisnoc_router_1_out4_vc [get_bd_pins axisnoc_interface_1/m00_noc_vc] [get_bd_pins axisnoc_router_1/out4_vc]
  connect_bd_net -net axisnoc_router_2_in1_ready [get_bd_pins axisnoc_router_2/in1_ready] [get_bd_pins axisnoc_router_3/out1_ready]
  connect_bd_net -net axisnoc_router_2_in2_ready [get_bd_pins axisnoc_router_0/out2_ready] [get_bd_pins axisnoc_router_2/in2_ready]
  connect_bd_net -net axisnoc_router_2_in3_ready [get_bd_pins axisnoc_interface_4/s00_noc_ready] [get_bd_pins axisnoc_router_2/in3_ready]
  connect_bd_net -net axisnoc_router_2_in4_ready [get_bd_pins axisnoc_interface_2/s00_noc_ready] [get_bd_pins axisnoc_router_2/in4_ready]
  connect_bd_net -net axisnoc_router_2_out0_data [get_bd_pins axisnoc_router_2/out0_data] [get_bd_pins axisnoc_router_3/in0_data]
  connect_bd_net -net axisnoc_router_2_out0_valid [get_bd_pins axisnoc_router_2/out0_valid] [get_bd_pins axisnoc_router_3/in0_valid]
  connect_bd_net -net axisnoc_router_2_out0_vc [get_bd_pins axisnoc_router_2/out0_vc] [get_bd_pins axisnoc_router_3/in0_vc]
  connect_bd_net -net axisnoc_router_2_out2_data [get_bd_pins axisnoc_interface_4/m00_noc_data] [get_bd_pins axisnoc_router_2/out2_data]
  connect_bd_net -net axisnoc_router_2_out2_valid [get_bd_pins axisnoc_interface_4/m00_noc_valid] [get_bd_pins axisnoc_router_2/out2_valid]
  connect_bd_net -net axisnoc_router_2_out2_vc [get_bd_pins axisnoc_interface_4/m00_noc_vc] [get_bd_pins axisnoc_router_2/out2_vc]
  connect_bd_net -net axisnoc_router_2_out3_data [get_bd_pins axisnoc_router_0/in3_data] [get_bd_pins axisnoc_router_2/out3_data]
  connect_bd_net -net axisnoc_router_2_out3_valid [get_bd_pins axisnoc_router_0/in3_valid] [get_bd_pins axisnoc_router_2/out3_valid]
  connect_bd_net -net axisnoc_router_2_out3_vc [get_bd_pins axisnoc_router_0/in3_vc] [get_bd_pins axisnoc_router_2/out3_vc]
  connect_bd_net -net axisnoc_router_2_out4_data [get_bd_pins axisnoc_interface_2/m00_noc_data] [get_bd_pins axisnoc_router_2/out4_data]
  connect_bd_net -net axisnoc_router_2_out4_valid [get_bd_pins axisnoc_interface_2/m00_noc_valid] [get_bd_pins axisnoc_router_2/out4_valid]
  connect_bd_net -net axisnoc_router_2_out4_vc [get_bd_pins axisnoc_interface_2/m00_noc_vc] [get_bd_pins axisnoc_router_2/out4_vc]
  connect_bd_net -net axisnoc_router_3_in0_ready [get_bd_pins axisnoc_router_2/out0_ready] [get_bd_pins axisnoc_router_3/in0_ready]
  connect_bd_net -net axisnoc_router_3_in2_ready [get_bd_pins axisnoc_router_1/out2_ready] [get_bd_pins axisnoc_router_3/in2_ready]
  connect_bd_net -net axisnoc_router_3_in3_ready [get_bd_pins axisnoc_interface_5/s00_noc_ready] [get_bd_pins axisnoc_router_3/in3_ready]
  connect_bd_net -net axisnoc_router_3_in4_ready [get_bd_pins axisnoc_interface_3/s00_noc_ready] [get_bd_pins axisnoc_router_3/in4_ready]
  connect_bd_net -net axisnoc_router_3_out1_data [get_bd_pins axisnoc_router_2/in1_data] [get_bd_pins axisnoc_router_3/out1_data]
  connect_bd_net -net axisnoc_router_3_out1_valid [get_bd_pins axisnoc_router_2/in1_valid] [get_bd_pins axisnoc_router_3/out1_valid]
  connect_bd_net -net axisnoc_router_3_out1_vc [get_bd_pins axisnoc_router_2/in1_vc] [get_bd_pins axisnoc_router_3/out1_vc]
  connect_bd_net -net axisnoc_router_3_out2_data [get_bd_pins axisnoc_interface_5/m00_noc_data] [get_bd_pins axisnoc_router_3/out2_data]
  connect_bd_net -net axisnoc_router_3_out2_valid [get_bd_pins axisnoc_interface_5/m00_noc_valid] [get_bd_pins axisnoc_router_3/out2_valid]
  connect_bd_net -net axisnoc_router_3_out2_vc [get_bd_pins axisnoc_interface_5/m00_noc_vc] [get_bd_pins axisnoc_router_3/out2_vc]
  connect_bd_net -net axisnoc_router_3_out3_data [get_bd_pins axisnoc_router_1/in3_data] [get_bd_pins axisnoc_router_3/out3_data]
  connect_bd_net -net axisnoc_router_3_out3_valid [get_bd_pins axisnoc_router_1/in3_valid] [get_bd_pins axisnoc_router_3/out3_valid]
  connect_bd_net -net axisnoc_router_3_out3_vc [get_bd_pins axisnoc_router_1/in3_vc] [get_bd_pins axisnoc_router_3/out3_vc]
  connect_bd_net -net axisnoc_router_3_out4_data [get_bd_pins axisnoc_interface_3/m00_noc_data] [get_bd_pins axisnoc_router_3/out4_data]
  connect_bd_net -net axisnoc_router_3_out4_valid [get_bd_pins axisnoc_interface_3/m00_noc_valid] [get_bd_pins axisnoc_router_3/out4_valid]
  connect_bd_net -net axisnoc_router_3_out4_vc [get_bd_pins axisnoc_interface_3/m00_noc_vc] [get_bd_pins axisnoc_router_3/out4_vc]
  connect_bd_net -net clk_1 [get_bd_pins clk] [get_bd_pins axisnoc_router_0/clk] [get_bd_pins axisnoc_router_1/clk] [get_bd_pins axisnoc_router_2/clk] [get_bd_pins axisnoc_router_3/clk]
  connect_bd_net -net m00_axis_aclk_1 [get_bd_pins m00_axis_aclk] [get_bd_pins axisnoc_interface_0/m00_axis_aclk] [get_bd_pins axisnoc_interface_0/s00_axis_aclk] [get_bd_pins axisnoc_interface_1/m00_axis_aclk] [get_bd_pins axisnoc_interface_1/s00_axis_aclk] [get_bd_pins axisnoc_interface_2/m00_axis_aclk] [get_bd_pins axisnoc_interface_2/s00_axis_aclk] [get_bd_pins axisnoc_interface_3/m00_axis_aclk] [get_bd_pins axisnoc_interface_3/s00_axis_aclk] [get_bd_pins axisnoc_interface_4/m00_axis_aclk] [get_bd_pins axisnoc_interface_4/s00_axis_aclk] [get_bd_pins axisnoc_interface_5/m00_axis_aclk] [get_bd_pins axisnoc_interface_5/s00_axis_aclk] [get_bd_pins axisnoc_interface_6/m00_axis_aclk] [get_bd_pins axisnoc_interface_6/s00_axis_aclk] [get_bd_pins axisnoc_interface_7/m00_axis_aclk] [get_bd_pins axisnoc_interface_7/s00_axis_aclk]
  connect_bd_net -net m00_axis_aresetn_1 [get_bd_pins m00_axis_aresetn] [get_bd_pins axisnoc_interface_0/m00_axis_aresetn] [get_bd_pins axisnoc_interface_0/s00_axis_aresetn] [get_bd_pins axisnoc_interface_1/m00_axis_aresetn] [get_bd_pins axisnoc_interface_1/s00_axis_aresetn] [get_bd_pins axisnoc_interface_2/m00_axis_aresetn] [get_bd_pins axisnoc_interface_2/s00_axis_aresetn] [get_bd_pins axisnoc_interface_3/m00_axis_aresetn] [get_bd_pins axisnoc_interface_3/s00_axis_aresetn] [get_bd_pins axisnoc_interface_4/m00_axis_aresetn] [get_bd_pins axisnoc_interface_4/s00_axis_aresetn] [get_bd_pins axisnoc_interface_5/m00_axis_aresetn] [get_bd_pins axisnoc_interface_5/s00_axis_aresetn] [get_bd_pins axisnoc_interface_6/m00_axis_aresetn] [get_bd_pins axisnoc_interface_6/s00_axis_aresetn] [get_bd_pins axisnoc_interface_7/m00_axis_aresetn] [get_bd_pins axisnoc_interface_7/s00_axis_aresetn]
  connect_bd_net -net mon_addr_1 [get_bd_pins mon_addr] [get_bd_pins axisnoc_router_0/mon_addr]
  connect_bd_net -net rst_1 [get_bd_pins rst] [get_bd_pins axisnoc_router_0/rst] [get_bd_pins axisnoc_router_1/rst] [get_bd_pins axisnoc_router_2/rst] [get_bd_pins axisnoc_router_3/rst]
  connect_bd_net -net xlconstant_0_const [get_bd_pins axisnoc_router_0/in0_data] [get_bd_pins axisnoc_router_0/in0_valid] [get_bd_pins axisnoc_router_0/in0_vc] [get_bd_pins axisnoc_router_0/out1_ready] [get_bd_pins xlconstant_0/const]
  connect_bd_net -net xlconstant_1_const [get_bd_pins axisnoc_router_1/in1_data] [get_bd_pins axisnoc_router_1/in1_valid] [get_bd_pins axisnoc_router_1/in1_vc] [get_bd_pins axisnoc_router_1/mon_addr] [get_bd_pins axisnoc_router_1/out0_ready] [get_bd_pins xlconstant_1/const]
  connect_bd_net -net xlconstant_2_const [get_bd_pins axisnoc_router_2/in0_data] [get_bd_pins axisnoc_router_2/in0_valid] [get_bd_pins axisnoc_router_2/in0_vc] [get_bd_pins axisnoc_router_2/mon_addr] [get_bd_pins axisnoc_router_2/out1_ready] [get_bd_pins xlconstant_2/const]
  connect_bd_net -net xlconstant_3_const [get_bd_pins axisnoc_router_3/in1_data] [get_bd_pins axisnoc_router_3/in1_valid] [get_bd_pins axisnoc_router_3/in1_vc] [get_bd_pins axisnoc_router_3/mon_addr] [get_bd_pins axisnoc_router_3/out0_ready] [get_bd_pins xlconstant_3/const]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: microblaze_7_local_memory
proc create_hier_cell_microblaze_7_local_memory { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_microblaze_7_local_memory() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  # Create pins
  create_bd_pin -dir I LMB_Clk
  create_bd_pin -dir I -from 0 -to 0 LMB_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.1 lmb_bram ]
  set_property -dict [ list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.use_bram_block {BRAM_Controller}  ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_7_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_7_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_7_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_7_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_7_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_7_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net microblaze_7_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]
  connect_bd_net -net microblaze_7_LMB_Rst [get_bd_pins LMB_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: microblaze_6_local_memory
proc create_hier_cell_microblaze_6_local_memory { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_microblaze_6_local_memory() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  # Create pins
  create_bd_pin -dir I LMB_Clk
  create_bd_pin -dir I -from 0 -to 0 LMB_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.1 lmb_bram ]
  set_property -dict [ list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.use_bram_block {BRAM_Controller}  ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_6_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_6_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_6_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_6_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_6_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_6_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net microblaze_6_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]
  connect_bd_net -net microblaze_6_LMB_Rst [get_bd_pins LMB_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: microblaze_5_local_memory
proc create_hier_cell_microblaze_5_local_memory { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_microblaze_5_local_memory() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  # Create pins
  create_bd_pin -dir I LMB_Clk
  create_bd_pin -dir I -from 0 -to 0 LMB_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.1 lmb_bram ]
  set_property -dict [ list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.use_bram_block {BRAM_Controller}  ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_5_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_5_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_5_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_5_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_5_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_5_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net microblaze_5_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]
  connect_bd_net -net microblaze_5_LMB_Rst [get_bd_pins LMB_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: microblaze_4_local_memory
proc create_hier_cell_microblaze_4_local_memory { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_microblaze_4_local_memory() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  # Create pins
  create_bd_pin -dir I LMB_Clk
  create_bd_pin -dir I -from 0 -to 0 LMB_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.1 lmb_bram ]
  set_property -dict [ list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.use_bram_block {BRAM_Controller}  ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_4_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_4_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_4_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_4_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_4_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_4_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net microblaze_4_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]
  connect_bd_net -net microblaze_4_LMB_Rst [get_bd_pins LMB_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: microblaze_3_local_memory
proc create_hier_cell_microblaze_3_local_memory { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_microblaze_3_local_memory() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  # Create pins
  create_bd_pin -dir I LMB_Clk
  create_bd_pin -dir I -from 0 -to 0 LMB_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.1 lmb_bram ]
  set_property -dict [ list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.use_bram_block {BRAM_Controller}  ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_3_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_3_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_3_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_3_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_3_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_3_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net microblaze_3_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]
  connect_bd_net -net microblaze_3_LMB_Rst [get_bd_pins LMB_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: microblaze_2_local_memory
proc create_hier_cell_microblaze_2_local_memory { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_microblaze_2_local_memory() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  # Create pins
  create_bd_pin -dir I LMB_Clk
  create_bd_pin -dir I -from 0 -to 0 LMB_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.1 lmb_bram ]
  set_property -dict [ list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.use_bram_block {BRAM_Controller}  ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_2_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_2_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_2_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_2_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_2_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_2_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net microblaze_2_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]
  connect_bd_net -net microblaze_2_LMB_Rst [get_bd_pins LMB_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: microblaze_1_local_memory
proc create_hier_cell_microblaze_1_local_memory { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_microblaze_1_local_memory() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  # Create pins
  create_bd_pin -dir I LMB_Clk
  create_bd_pin -dir I -from 0 -to 0 LMB_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.1 lmb_bram ]
  set_property -dict [ list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.use_bram_block {BRAM_Controller}  ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_1_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_1_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_1_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_1_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_1_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_1_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net microblaze_1_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]
  connect_bd_net -net microblaze_1_LMB_Rst [get_bd_pins LMB_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: microblaze_0_local_memory
proc create_hier_cell_microblaze_0_local_memory { parentCell nameHier } {

  if { $parentCell eq "" || $nameHier eq "" } {
     puts "ERROR: create_hier_cell_microblaze_0_local_memory() - Empty argument(s)!"
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 DLMB
  create_bd_intf_pin -mode MirroredMaster -vlnv xilinx.com:interface:lmb_rtl:1.0 ILMB

  # Create pins
  create_bd_pin -dir I LMB_Clk
  create_bd_pin -dir I -from 0 -to 0 LMB_Rst

  # Create instance: dlmb_bram_if_cntlr, and set properties
  set dlmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 dlmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $dlmb_bram_if_cntlr

  # Create instance: dlmb_v10, and set properties
  set dlmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 dlmb_v10 ]

  # Create instance: ilmb_bram_if_cntlr, and set properties
  set ilmb_bram_if_cntlr [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_bram_if_cntlr:4.0 ilmb_bram_if_cntlr ]
  set_property -dict [ list CONFIG.C_ECC {0}  ] $ilmb_bram_if_cntlr

  # Create instance: ilmb_v10, and set properties
  set ilmb_v10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:lmb_v10:3.0 ilmb_v10 ]

  # Create instance: lmb_bram, and set properties
  set lmb_bram [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.1 lmb_bram ]
  set_property -dict [ list CONFIG.Memory_Type {True_Dual_Port_RAM} CONFIG.use_bram_block {BRAM_Controller}  ] $lmb_bram

  # Create interface connections
  connect_bd_intf_net -intf_net microblaze_0_dlmb [get_bd_intf_pins DLMB] [get_bd_intf_pins dlmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_bus [get_bd_intf_pins dlmb_bram_if_cntlr/SLMB] [get_bd_intf_pins dlmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_dlmb_cntlr [get_bd_intf_pins dlmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTA]
  connect_bd_intf_net -intf_net microblaze_0_ilmb [get_bd_intf_pins ILMB] [get_bd_intf_pins ilmb_v10/LMB_M]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_bus [get_bd_intf_pins ilmb_bram_if_cntlr/SLMB] [get_bd_intf_pins ilmb_v10/LMB_Sl_0]
  connect_bd_intf_net -intf_net microblaze_0_ilmb_cntlr [get_bd_intf_pins ilmb_bram_if_cntlr/BRAM_PORT] [get_bd_intf_pins lmb_bram/BRAM_PORTB]

  # Create port connections
  connect_bd_net -net microblaze_0_Clk [get_bd_pins LMB_Clk] [get_bd_pins dlmb_bram_if_cntlr/LMB_Clk] [get_bd_pins dlmb_v10/LMB_Clk] [get_bd_pins ilmb_bram_if_cntlr/LMB_Clk] [get_bd_pins ilmb_v10/LMB_Clk]
  connect_bd_net -net microblaze_0_LMB_Rst [get_bd_pins LMB_Rst] [get_bd_pins dlmb_bram_if_cntlr/LMB_Rst] [get_bd_pins dlmb_v10/SYS_Rst] [get_bd_pins ilmb_bram_if_cntlr/LMB_Rst] [get_bd_pins ilmb_v10/SYS_Rst]
  
  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     puts "ERROR: Unable to find parent cell <$parentCell>!"
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     puts "ERROR: Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set CLK_IN1_D [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 CLK_IN1_D ]
  set_property -dict [ list CONFIG.FREQ_HZ {200000000}  ] $CLK_IN1_D
  set uart_rtl [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:uart_rtl:1.0 uart_rtl ]

  # Create ports
  set reset [ create_bd_port -dir I -type rst reset ]
  set_property -dict [ list CONFIG.POLARITY {ACTIVE_HIGH}  ] $reset

  # Create instance: axi_crossbar_0, and set properties
  set axi_crossbar_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_crossbar:2.1 axi_crossbar_0 ]
  set_property -dict [ list CONFIG.STRATEGY {1}  ] $axi_crossbar_0

  # Create instance: axi_gpio_0, and set properties
  set axi_gpio_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_gpio:2.0 axi_gpio_0 ]
  set_property -dict [ list CONFIG.C_ALL_INPUTS_2 {1} CONFIG.C_ALL_OUTPUTS {1} CONFIG.C_GPIO_WIDTH {12} CONFIG.C_IS_DUAL {1}  ] $axi_gpio_0

  # Create instance: axi_uartlite_0, and set properties
  set axi_uartlite_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_uartlite:2.0 axi_uartlite_0 ]
  set_property -dict [ list CONFIG.C_BAUDRATE {115200}  ] $axi_uartlite_0

  # Create instance: clk_wiz_1, and set properties
  set clk_wiz_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:5.1 clk_wiz_1 ]
  set_property -dict [ list CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {100.000} CONFIG.PRIM_IN_FREQ {200.000} CONFIG.PRIM_SOURCE {Differential_clock_capable_pin}  ] $clk_wiz_1

  # Create instance: mdm_1, and set properties
  set mdm_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:mdm:3.0 mdm_1 ]
  set_property -dict [ list CONFIG.C_MB_DBG_PORTS {8} CONFIG.C_USE_UART {0}  ] $mdm_1

  # Create instance: microblaze_0, and set properties
  set microblaze_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:9.2 microblaze_0 ]
  set_property -dict [ list CONFIG.C_DEBUG_ENABLED {1} CONFIG.C_D_AXI {1} CONFIG.C_D_LMB {1} CONFIG.C_FAULT_TOLERANT {0} CONFIG.C_FSL_LINKS {1} CONFIG.C_I_LMB {1} CONFIG.C_PVR {1} CONFIG.C_USE_BARREL {1} CONFIG.C_USE_BRANCH_TARGET_CACHE {1} CONFIG.C_USE_EXTENDED_FSL_INSTR {1} CONFIG.C_USE_FPU {1} CONFIG.C_USE_HW_MUL {1} CONFIG.C_USE_INTERRUPT {0}  ] $microblaze_0

  # Create instance: microblaze_0_local_memory
  create_hier_cell_microblaze_0_local_memory [current_bd_instance .] microblaze_0_local_memory

  # Create instance: microblaze_1, and set properties
  set microblaze_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:9.2 microblaze_1 ]
  set_property -dict [ list CONFIG.C_DEBUG_ENABLED {1} CONFIG.C_D_LMB {1} CONFIG.C_FAULT_TOLERANT {0} CONFIG.C_FSL_LINKS {1} CONFIG.C_I_LMB {1} CONFIG.C_PVR {1} CONFIG.C_PVR_USER1 {0x01} CONFIG.C_USE_BARREL {1} CONFIG.C_USE_BRANCH_TARGET_CACHE {1} CONFIG.C_USE_EXTENDED_FSL_INSTR {1} CONFIG.C_USE_FPU {1} CONFIG.C_USE_HW_MUL {1} CONFIG.C_USE_INTERRUPT {0}  ] $microblaze_1

  # Create instance: microblaze_1_local_memory
  create_hier_cell_microblaze_1_local_memory [current_bd_instance .] microblaze_1_local_memory

  # Create instance: microblaze_2, and set properties
  set microblaze_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:9.2 microblaze_2 ]
  set_property -dict [ list CONFIG.C_DEBUG_ENABLED {1} CONFIG.C_D_LMB {1} CONFIG.C_FAULT_TOLERANT {0} CONFIG.C_FSL_LINKS {1} CONFIG.C_I_LMB {1} CONFIG.C_PVR {1} CONFIG.C_PVR_USER1 {0x10} CONFIG.C_USE_BARREL {1} CONFIG.C_USE_BRANCH_TARGET_CACHE {1} CONFIG.C_USE_EXTENDED_FSL_INSTR {1} CONFIG.C_USE_FPU {1} CONFIG.C_USE_HW_MUL {1} CONFIG.C_USE_INTERRUPT {0}  ] $microblaze_2

  # Create instance: microblaze_2_local_memory
  create_hier_cell_microblaze_2_local_memory [current_bd_instance .] microblaze_2_local_memory

  # Create instance: microblaze_3, and set properties
  set microblaze_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:9.2 microblaze_3 ]
  set_property -dict [ list CONFIG.C_DEBUG_ENABLED {1} CONFIG.C_D_LMB {1} CONFIG.C_FAULT_TOLERANT {0} CONFIG.C_FSL_LINKS {1} CONFIG.C_I_LMB {1} CONFIG.C_PVR {1} CONFIG.C_PVR_USER1 {0x11} CONFIG.C_USE_BARREL {1} CONFIG.C_USE_BRANCH_TARGET_CACHE {1} CONFIG.C_USE_EXTENDED_FSL_INSTR {1} CONFIG.C_USE_FPU {1} CONFIG.C_USE_HW_MUL {1} CONFIG.C_USE_INTERRUPT {0}  ] $microblaze_3

  # Create instance: microblaze_3_local_memory
  create_hier_cell_microblaze_3_local_memory [current_bd_instance .] microblaze_3_local_memory

  # Create instance: microblaze_4, and set properties
  set microblaze_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:9.2 microblaze_4 ]
  set_property -dict [ list CONFIG.C_DEBUG_ENABLED {1} CONFIG.C_D_LMB {1} CONFIG.C_FAULT_TOLERANT {0} CONFIG.C_FSL_LINKS {1} CONFIG.C_I_LMB {1} CONFIG.C_PVR {1} CONFIG.C_PVR_USER1 {0x20} CONFIG.C_USE_BARREL {1} CONFIG.C_USE_BRANCH_TARGET_CACHE {1} CONFIG.C_USE_EXTENDED_FSL_INSTR {1} CONFIG.C_USE_FPU {1} CONFIG.C_USE_HW_MUL {1} CONFIG.C_USE_INTERRUPT {0}  ] $microblaze_4

  # Create instance: microblaze_4_local_memory
  create_hier_cell_microblaze_4_local_memory [current_bd_instance .] microblaze_4_local_memory

  # Create instance: microblaze_5, and set properties
  set microblaze_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:9.2 microblaze_5 ]
  set_property -dict [ list CONFIG.C_DEBUG_ENABLED {1} CONFIG.C_D_LMB {1} CONFIG.C_FAULT_TOLERANT {0} CONFIG.C_FSL_LINKS {1} CONFIG.C_I_LMB {1} CONFIG.C_PVR {1} CONFIG.C_PVR_USER1 {0x21} CONFIG.C_USE_BARREL {1} CONFIG.C_USE_BRANCH_TARGET_CACHE {1} CONFIG.C_USE_EXTENDED_FSL_INSTR {1} CONFIG.C_USE_FPU {1} CONFIG.C_USE_HW_MUL {1} CONFIG.C_USE_INTERRUPT {0}  ] $microblaze_5

  # Create instance: microblaze_5_local_memory
  create_hier_cell_microblaze_5_local_memory [current_bd_instance .] microblaze_5_local_memory

  # Create instance: microblaze_6, and set properties
  set microblaze_6 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:9.2 microblaze_6 ]
  set_property -dict [ list CONFIG.C_DEBUG_ENABLED {1} CONFIG.C_D_LMB {1} CONFIG.C_FAULT_TOLERANT {0} CONFIG.C_FSL_LINKS {1} CONFIG.C_I_LMB {1} CONFIG.C_PVR {1} CONFIG.C_PVR_USER1 {0x30} CONFIG.C_USE_BARREL {1} CONFIG.C_USE_BRANCH_TARGET_CACHE {1} CONFIG.C_USE_EXTENDED_FSL_INSTR {1} CONFIG.C_USE_FPU {1} CONFIG.C_USE_HW_MUL {1} CONFIG.C_USE_INTERRUPT {0}  ] $microblaze_6

  # Create instance: microblaze_6_local_memory
  create_hier_cell_microblaze_6_local_memory [current_bd_instance .] microblaze_6_local_memory

  # Create instance: microblaze_7, and set properties
  set microblaze_7 [ create_bd_cell -type ip -vlnv xilinx.com:ip:microblaze:9.2 microblaze_7 ]
  set_property -dict [ list CONFIG.C_DEBUG_ENABLED {1} CONFIG.C_D_LMB {1} CONFIG.C_FAULT_TOLERANT {0} CONFIG.C_FSL_LINKS {1} CONFIG.C_I_LMB {1} CONFIG.C_PVR {1} CONFIG.C_PVR_USER1 {0x31} CONFIG.C_USE_BARREL {1} CONFIG.C_USE_BRANCH_TARGET_CACHE {1} CONFIG.C_USE_EXTENDED_FSL_INSTR {1} CONFIG.C_USE_FPU {1} CONFIG.C_USE_HW_MUL {1} CONFIG.C_USE_INTERRUPT {0}  ] $microblaze_7

  # Create instance: microblaze_7_local_memory
  create_hier_cell_microblaze_7_local_memory [current_bd_instance .] microblaze_7_local_memory

  # Create instance: noc_2x2
  create_hier_cell_noc_2x2 [current_bd_instance .] noc_2x2

  # Create instance: rst_clk_wiz_1_100M, and set properties
  set rst_clk_wiz_1_100M [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 rst_clk_wiz_1_100M ]

  # Create interface connections
  connect_bd_intf_net -intf_net CLK_IN1_D_1 [get_bd_intf_ports CLK_IN1_D] [get_bd_intf_pins clk_wiz_1/CLK_IN1_D]
  connect_bd_intf_net -intf_net axi_crossbar_0_M00_AXI [get_bd_intf_pins axi_crossbar_0/M00_AXI] [get_bd_intf_pins axi_uartlite_0/S_AXI]
  connect_bd_intf_net -intf_net axi_crossbar_0_M01_AXI [get_bd_intf_pins axi_crossbar_0/M01_AXI] [get_bd_intf_pins axi_gpio_0/S_AXI]
  connect_bd_intf_net -intf_net axi_uartlite_0_UART [get_bd_intf_ports uart_rtl] [get_bd_intf_pins axi_uartlite_0/UART]
  connect_bd_intf_net -intf_net microblaze_0_M0_AXIS [get_bd_intf_pins microblaze_0/M0_AXIS] [get_bd_intf_pins noc_2x2/S00_AXIS7]
  connect_bd_intf_net -intf_net microblaze_0_M_AXI_DP [get_bd_intf_pins axi_crossbar_0/S00_AXI] [get_bd_intf_pins microblaze_0/M_AXI_DP]
  connect_bd_intf_net -intf_net microblaze_0_debug [get_bd_intf_pins mdm_1/MBDEBUG_0] [get_bd_intf_pins microblaze_0/DEBUG]
  connect_bd_intf_net -intf_net microblaze_0_dlmb__1 [get_bd_intf_pins microblaze_0/DLMB] [get_bd_intf_pins microblaze_0_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_0_ilmb__1 [get_bd_intf_pins microblaze_0/ILMB] [get_bd_intf_pins microblaze_0_local_memory/ILMB]
  connect_bd_intf_net -intf_net microblaze_1_M0_AXIS [get_bd_intf_pins microblaze_1/M0_AXIS] [get_bd_intf_pins noc_2x2/S00_AXIS6]
  connect_bd_intf_net -intf_net microblaze_1_debug [get_bd_intf_pins mdm_1/MBDEBUG_1] [get_bd_intf_pins microblaze_1/DEBUG]
  connect_bd_intf_net -intf_net microblaze_1_dlmb__1 [get_bd_intf_pins microblaze_1/DLMB] [get_bd_intf_pins microblaze_1_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_1_ilmb__1 [get_bd_intf_pins microblaze_1/ILMB] [get_bd_intf_pins microblaze_1_local_memory/ILMB]
  connect_bd_intf_net -intf_net microblaze_2_M0_AXIS [get_bd_intf_pins microblaze_2/M0_AXIS] [get_bd_intf_pins noc_2x2/S00_AXIS]
  connect_bd_intf_net -intf_net microblaze_2_debug [get_bd_intf_pins mdm_1/MBDEBUG_2] [get_bd_intf_pins microblaze_2/DEBUG]
  connect_bd_intf_net -intf_net microblaze_2_dlmb__1 [get_bd_intf_pins microblaze_2/DLMB] [get_bd_intf_pins microblaze_2_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_2_ilmb__1 [get_bd_intf_pins microblaze_2/ILMB] [get_bd_intf_pins microblaze_2_local_memory/ILMB]
  connect_bd_intf_net -intf_net microblaze_3_M0_AXIS [get_bd_intf_pins microblaze_3/M0_AXIS] [get_bd_intf_pins noc_2x2/S00_AXIS1]
  connect_bd_intf_net -intf_net microblaze_3_debug [get_bd_intf_pins mdm_1/MBDEBUG_3] [get_bd_intf_pins microblaze_3/DEBUG]
  connect_bd_intf_net -intf_net microblaze_3_dlmb__1 [get_bd_intf_pins microblaze_3/DLMB] [get_bd_intf_pins microblaze_3_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_3_ilmb__1 [get_bd_intf_pins microblaze_3/ILMB] [get_bd_intf_pins microblaze_3_local_memory/ILMB]
  connect_bd_intf_net -intf_net microblaze_4_M0_AXIS [get_bd_intf_pins microblaze_4/M0_AXIS] [get_bd_intf_pins noc_2x2/S00_AXIS2]
  connect_bd_intf_net -intf_net microblaze_4_debug [get_bd_intf_pins mdm_1/MBDEBUG_4] [get_bd_intf_pins microblaze_4/DEBUG]
  connect_bd_intf_net -intf_net microblaze_4_dlmb__1 [get_bd_intf_pins microblaze_4/DLMB] [get_bd_intf_pins microblaze_4_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_4_ilmb__1 [get_bd_intf_pins microblaze_4/ILMB] [get_bd_intf_pins microblaze_4_local_memory/ILMB]
  connect_bd_intf_net -intf_net microblaze_5_M0_AXIS [get_bd_intf_pins microblaze_5/M0_AXIS] [get_bd_intf_pins noc_2x2/S00_AXIS3]
  connect_bd_intf_net -intf_net microblaze_5_debug [get_bd_intf_pins mdm_1/MBDEBUG_5] [get_bd_intf_pins microblaze_5/DEBUG]
  connect_bd_intf_net -intf_net microblaze_5_dlmb__1 [get_bd_intf_pins microblaze_5/DLMB] [get_bd_intf_pins microblaze_5_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_5_ilmb__1 [get_bd_intf_pins microblaze_5/ILMB] [get_bd_intf_pins microblaze_5_local_memory/ILMB]
  connect_bd_intf_net -intf_net microblaze_6_M0_AXIS [get_bd_intf_pins microblaze_6/M0_AXIS] [get_bd_intf_pins noc_2x2/S00_AXIS4]
  connect_bd_intf_net -intf_net microblaze_6_debug [get_bd_intf_pins mdm_1/MBDEBUG_6] [get_bd_intf_pins microblaze_6/DEBUG]
  connect_bd_intf_net -intf_net microblaze_6_dlmb__1 [get_bd_intf_pins microblaze_6/DLMB] [get_bd_intf_pins microblaze_6_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_6_ilmb__1 [get_bd_intf_pins microblaze_6/ILMB] [get_bd_intf_pins microblaze_6_local_memory/ILMB]
  connect_bd_intf_net -intf_net microblaze_7_M0_AXIS [get_bd_intf_pins microblaze_7/M0_AXIS] [get_bd_intf_pins noc_2x2/S00_AXIS5]
  connect_bd_intf_net -intf_net microblaze_7_debug [get_bd_intf_pins mdm_1/MBDEBUG_7] [get_bd_intf_pins microblaze_7/DEBUG]
  connect_bd_intf_net -intf_net microblaze_7_dlmb__1 [get_bd_intf_pins microblaze_7/DLMB] [get_bd_intf_pins microblaze_7_local_memory/DLMB]
  connect_bd_intf_net -intf_net microblaze_7_ilmb__1 [get_bd_intf_pins microblaze_7/ILMB] [get_bd_intf_pins microblaze_7_local_memory/ILMB]
  connect_bd_intf_net -intf_net noc_2x2_M00_AXIS [get_bd_intf_pins microblaze_2/S0_AXIS] [get_bd_intf_pins noc_2x2/M00_AXIS]
  connect_bd_intf_net -intf_net noc_2x2_M00_AXIS1 [get_bd_intf_pins microblaze_3/S0_AXIS] [get_bd_intf_pins noc_2x2/M00_AXIS1]
  connect_bd_intf_net -intf_net noc_2x2_M00_AXIS2 [get_bd_intf_pins microblaze_4/S0_AXIS] [get_bd_intf_pins noc_2x2/M00_AXIS2]
  connect_bd_intf_net -intf_net noc_2x2_M00_AXIS3 [get_bd_intf_pins microblaze_5/S0_AXIS] [get_bd_intf_pins noc_2x2/M00_AXIS3]
  connect_bd_intf_net -intf_net noc_2x2_M00_AXIS4 [get_bd_intf_pins microblaze_6/S0_AXIS] [get_bd_intf_pins noc_2x2/M00_AXIS4]
  connect_bd_intf_net -intf_net noc_2x2_M00_AXIS5 [get_bd_intf_pins microblaze_7/S0_AXIS] [get_bd_intf_pins noc_2x2/M00_AXIS5]
  connect_bd_intf_net -intf_net noc_2x2_M00_AXIS6 [get_bd_intf_pins microblaze_1/S0_AXIS] [get_bd_intf_pins noc_2x2/M00_AXIS6]
  connect_bd_intf_net -intf_net noc_2x2_M00_AXIS7 [get_bd_intf_pins microblaze_0/S0_AXIS] [get_bd_intf_pins noc_2x2/M00_AXIS7]

  # Create port connections
  connect_bd_net -net axi_gpio_0_gpio_io_o [get_bd_pins axi_gpio_0/gpio_io_o] [get_bd_pins noc_2x2/mon_addr]
  connect_bd_net -net clk_wiz_1_locked [get_bd_pins clk_wiz_1/locked] [get_bd_pins rst_clk_wiz_1_100M/dcm_locked]
  connect_bd_net -net mdm_1_debug_sys_rst [get_bd_pins mdm_1/Debug_SYS_Rst] [get_bd_pins rst_clk_wiz_1_100M/mb_debug_sys_rst]
  connect_bd_net -net microblaze_7_Clk [get_bd_pins axi_crossbar_0/aclk] [get_bd_pins axi_gpio_0/s_axi_aclk] [get_bd_pins axi_uartlite_0/s_axi_aclk] [get_bd_pins clk_wiz_1/clk_out1] [get_bd_pins microblaze_0/Clk] [get_bd_pins microblaze_0_local_memory/LMB_Clk] [get_bd_pins microblaze_1/Clk] [get_bd_pins microblaze_1_local_memory/LMB_Clk] [get_bd_pins microblaze_2/Clk] [get_bd_pins microblaze_2_local_memory/LMB_Clk] [get_bd_pins microblaze_3/Clk] [get_bd_pins microblaze_3_local_memory/LMB_Clk] [get_bd_pins microblaze_4/Clk] [get_bd_pins microblaze_4_local_memory/LMB_Clk] [get_bd_pins microblaze_5/Clk] [get_bd_pins microblaze_5_local_memory/LMB_Clk] [get_bd_pins microblaze_6/Clk] [get_bd_pins microblaze_6_local_memory/LMB_Clk] [get_bd_pins microblaze_7/Clk] [get_bd_pins microblaze_7_local_memory/LMB_Clk] [get_bd_pins noc_2x2/clk] [get_bd_pins noc_2x2/m00_axis_aclk] [get_bd_pins rst_clk_wiz_1_100M/slowest_sync_clk]
  connect_bd_net -net noc_2x2_mon_data [get_bd_pins axi_gpio_0/gpio2_io_i] [get_bd_pins noc_2x2/mon_data]
  connect_bd_net -net reset_1 [get_bd_ports reset] [get_bd_pins clk_wiz_1/reset] [get_bd_pins rst_clk_wiz_1_100M/ext_reset_in]
  connect_bd_net -net rst_clk_wiz_1_100M_bus_struct_reset [get_bd_pins microblaze_0_local_memory/LMB_Rst] [get_bd_pins microblaze_1_local_memory/LMB_Rst] [get_bd_pins microblaze_2_local_memory/LMB_Rst] [get_bd_pins microblaze_3_local_memory/LMB_Rst] [get_bd_pins microblaze_4_local_memory/LMB_Rst] [get_bd_pins microblaze_5_local_memory/LMB_Rst] [get_bd_pins microblaze_6_local_memory/LMB_Rst] [get_bd_pins microblaze_7_local_memory/LMB_Rst] [get_bd_pins noc_2x2/rst] [get_bd_pins rst_clk_wiz_1_100M/bus_struct_reset]
  connect_bd_net -net rst_clk_wiz_1_100M_mb_reset [get_bd_pins microblaze_0/Reset] [get_bd_pins microblaze_1/Reset] [get_bd_pins microblaze_2/Reset] [get_bd_pins microblaze_3/Reset] [get_bd_pins microblaze_4/Reset] [get_bd_pins microblaze_5/Reset] [get_bd_pins microblaze_6/Reset] [get_bd_pins microblaze_7/Reset] [get_bd_pins rst_clk_wiz_1_100M/mb_reset]
  connect_bd_net -net rst_clk_wiz_1_100M_peripheral_aresetn [get_bd_pins axi_crossbar_0/aresetn] [get_bd_pins axi_gpio_0/s_axi_aresetn] [get_bd_pins axi_uartlite_0/s_axi_aresetn] [get_bd_pins noc_2x2/m00_axis_aresetn] [get_bd_pins rst_clk_wiz_1_100M/peripheral_aresetn]

  # Create address segments
  create_bd_addr_seg -range 0x10000 -offset 0x40000000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_gpio_0/S_AXI/Reg] SEG_axi_gpio_0_Reg
  create_bd_addr_seg -range 0x10000 -offset 0x40600000 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs axi_uartlite_0/S_AXI/Reg] SEG_axi_uartlite_0_Reg
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_0/Data] [get_bd_addr_segs microblaze_0_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_0/Instruction] [get_bd_addr_segs microblaze_0_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_1/Data] [get_bd_addr_segs microblaze_1_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_1/Instruction] [get_bd_addr_segs microblaze_1_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_2/Data] [get_bd_addr_segs microblaze_2_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_2/Instruction] [get_bd_addr_segs microblaze_2_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_3/Data] [get_bd_addr_segs microblaze_3_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_3/Instruction] [get_bd_addr_segs microblaze_3_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_4/Data] [get_bd_addr_segs microblaze_4_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_4/Instruction] [get_bd_addr_segs microblaze_4_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_5/Data] [get_bd_addr_segs microblaze_5_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_5/Instruction] [get_bd_addr_segs microblaze_5_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_6/Data] [get_bd_addr_segs microblaze_6_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_6/Instruction] [get_bd_addr_segs microblaze_6_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_7/Data] [get_bd_addr_segs microblaze_7_local_memory/dlmb_bram_if_cntlr/SLMB/Mem] SEG_dlmb_bram_if_cntlr_Mem
  create_bd_addr_seg -range 0x8000 -offset 0x0 [get_bd_addr_spaces microblaze_7/Instruction] [get_bd_addr_segs microblaze_7_local_memory/ilmb_bram_if_cntlr/SLMB/Mem] SEG_ilmb_bram_if_cntlr_Mem
  

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


